-- esciku.gw_swith definition

CREATE TABLE `gw_swith`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `swith_state` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
    `swith_code`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
    `flag`        int(11) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO esciku.gw_swith
    (id, swith_state, swith_code, flag)
VALUES (2, '10', 'synonym_doc', 0);


-- esciku.cere_elastic_synonym definition

CREATE TABLE `cere_elastic_synonym`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `synonym_docs` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
    `flag`         int(11) DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO esciku.cere_elastic_synonym
    (id, synonym_docs, flag)
VALUES (2, '苹果,iphone,手机,ipad,iphoneX', 0);
